# Willing URL Shortener

## Requirements
Preferably NodeJS >= 9.4.0, 9.0 and above should be fine

To modify, you will need CoffeeScript installed (`npm install -g coffeescript`)

## Installing
Simply clone this repository and run npm install
```bash
git clone git@bitbucket.org:mark_rawls/willing-coding-exercise.git && cd willing-coding-exercise && npm install
```

## Running
Once in the repository directory with dependencies installed, just run `npm start`. A server will open on `localhost:3000` with both the frontend and backend components available

## API
```javascript   
HTTP GET /v1/links =>
   response: { 'links': [{'target': 'example.com', 'hash': 'abcd1234'}, ...] }

HTTP GET /v1/:hash // redirects to target

HTTP POST /v1/links => // creates a new shortlink to provided url
   body: { 'url': '<target-url>' }
   response: { 'hash': '<short-link-hash>' }

HTTP PUT /v1/:hash => // updates the shortlink with provided url
	body: { 'url': '<target-url>' }
	response: { 'hash': '<short-link-hash>' }

HTTP DELETE /v1/:hash // deletes hash from database
```