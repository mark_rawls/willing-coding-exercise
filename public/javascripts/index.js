var refresh_table = function() {
	 $.get({
		  'url': '/table',
		  'success': function(data) {
				$('#table').html(data);
		  }
	 });
}

$(document).ready(function() {
	 refresh_table();
	 
	 $('#shortener').submit(function(e) {
		  e.preventDefault();
		  $.post({
				'url': '/v1/links',
				'data': {'url': $('#url-field').val()},
				'success': function(data) {
					 $('#url-field').val('');
					 refresh_table();
				}
		  });
	 });
});
