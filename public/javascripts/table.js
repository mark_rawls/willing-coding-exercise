$(document).ready(function() {
	 $('.delbutton').click(function(e) {
		  $.ajax({
				'url': '/v1/' + $(this).attr('data-hash'),
				'type': 'DELETE',
				'success': function(data) {
					 refresh_table();
				}
		  });
	 });

	 $('.targeturl').click(function(e) {
		  var targetlink = $(this).text();
		  var hash = $(this).attr('data-hash');
		  $(this).replaceWith('<form method="GET" id="url-update"><input name="url" type="text" class="form-control" value="'+targetlink+'" /><input name="hash" class="hashvalue" type="hidden" value="'+hash+'" /></form>');

		  // set our submit event after generating form so DOM binds properly
		  $('#url-update').submit(function(e) {
				e.preventDefault();
				$.ajax({
					 'url': '/v1/' + $(this).parent().find('.hashvalue').val(),
					 'type': 'PUT',
					 'data': { 'url': $(this).parent().find('.form-control').val() },
					 'success': function(data) {
						  console.log(data);
						  refresh_table();
					 }
				});
		  });
	 });
});
