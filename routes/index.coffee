express = require('express')
sqlite3 = require('sqlite3')
router = express.Router()

# sqlite connection
db = new sqlite3.Database 'links.db'

### Home page with DB controls ###
router.get '/', (req, res, next) ->
   res.render 'index', title: 'Management Panel | Willing Link Shortener'

### Generate table ###
router.get '/table', (req, res, next) ->
   db.all 'select * from links;', (err, rows) ->
      res.render 'table', items: rows

module.exports = router
