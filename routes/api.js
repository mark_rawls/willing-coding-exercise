// Generated by CoffeeScript 2.1.1
(function() {
  var db, delete_by_hash, express, insert_query, router, select_all, select_by_hash, shortid, sqlite3, update_query, valid_url_regex;

  express = require('express');

  shortid = require('shortid');

  sqlite3 = require('sqlite3');

  router = express.Router();

  // sqlite connection
  db = new sqlite3.Database('links.db');

  // url regex
  valid_url_regex = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/;

  // our sql queries
  insert_query = "insert into links values (?, ?);";

  update_query = "update links set target = ? where hash = ?;";

  select_by_hash = "select target from links where hash = ?;";

  delete_by_hash = "delete from links where hash = ?;";

  select_all = "select * from links;";

  /* Get list of shortlinks */
  router.get('/links', function(req, res, next) {
    return db.all(select_all, function(err, rows) {
      return res.json({
        'links': rows
      });
    });
  });

  /* Create a new shortlink */
  router.post('/links', function(req, res, next) {
    var hash, statement;
    // only run if the request is valid
    if ((req.body.url != null) && req.body.url.match(valid_url_regex) !== null) {
      statement = db.prepare(insert_query);
      hash = shortid.generate();
      statement.run(hash, req.body.url);
      statement.finalize();
      return res.json({
        'hash': hash
      });
    } else {
      // error handling
      return res.json({
        'hash': '',
        'error': 'No valid URL supplied in request body'
      });
    }
  });

  /* Redirect to shortlink */
  router.get('/:hash', function(req, res, next) {
    return db.get(select_by_hash, req.params.hash, function(err, row) {
      // for some indiscernible reason, the 'err' param is still null
      // even if the query has no results
      // so, we only check for results
      if (row != null) {
        return res.redirect(row.target);
      } else {
        return res.json({
          'error': 'No item found with hash ' + req.params.hash
        });
      }
    });
  });

  /* Update shortlink */
  router.put('/:hash', function(req, res, next) {
    var statement;
    // only run if the request is valid
    if ((req.body.url != null) && req.body.url.match(valid_url_regex) !== null) {
      statement = db.prepare(update_query);
      statement.run(req.body.url, req.params.hash);
      statement.finalize();
      return res.json({
        'hash': req.params.hash
      });
    } else {
      // error handling
      return res.json({
        'hash': '',
        'error': 'No valid URL supplied in request body'
      });
    }
  });

  /* Delete shortlink */
  router.delete('/:hash', function(req, res, next) {
    return db.run(delete_by_hash, req.params.hash, function(err) {
      return res.json({
        'errors': err,
        'hash': req.params.hash
      });
    });
  });

  module.exports = router;

}).call(this);
