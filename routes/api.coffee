express = require('express')
shortid = require('shortid')
sqlite3 = require('sqlite3')
router = express.Router()

# sqlite connection
db = new sqlite3.Database 'links.db'

# url regex
valid_url_regex = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/

# our sql queries
insert_query = "insert into links values (?, ?);"
update_query = "update links set target = ? where hash = ?;"
select_by_hash = "select target from links where hash = ?;"
delete_by_hash = "delete from links where hash = ?;"
select_all = "select * from links;"

### Get list of shortlinks ###
router.get '/links', (req, res, next) ->
   db.all select_all, (err, rows) ->
      res.json { 'links': rows }

### Create a new shortlink ###
router.post '/links', (req, res, next) ->
   # only run if the request is valid
   if req.body.url? && req.body.url.match(valid_url_regex) != null
      statement = db.prepare insert_query
      hash = shortid.generate()

      statement.run hash, req.body.url
      statement.finalize()

      res.json { 'hash': hash }
   else
      # error handling
      res.json { 'hash': '', 'error': 'No valid URL supplied in request body' }


### Redirect to shortlink ###
router.get '/:hash', (req, res, next) ->
   db.get select_by_hash, req.params.hash, (err, row) ->
      # for some indiscernible reason, the 'err' param is still null
      # even if the query has no results
      # so, we only check for results
      if row?
         res.redirect row.target
      else
         res.json { 'error': 'No item found with hash ' + req.params.hash }

### Update shortlink ###
router.put '/:hash', (req, res, next) ->
   # only run if the request is valid
   if req.body.url? && req.body.url.match(valid_url_regex) != null
      statement = db.prepare update_query

      statement.run req.body.url, req.params.hash
      statement.finalize()

      res.json { 'hash': req.params.hash }
   else
      # error handling
      res.json { 'hash': '', 'error': 'No valid URL supplied in request body' }

### Delete shortlink ###
router.delete '/:hash', (req, res, next) ->
   db.run delete_by_hash, req.params.hash, (err) ->
      res.json { 'errors': err, 'hash': req.params.hash }


module.exports = router
